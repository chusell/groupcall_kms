//
//  StartViewController.m
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "StartViewController.h"

#import "SFURenewalViewController.h"
#import "SFUNewViewController.h"

#import "SFUDefs.h"
#import "MagicalRecord/MagicalRecord.h"
#import "SFUCallLog+CoreDataProperties.h"

@interface StartViewController ()
#if PORTRAIT_MODE
<SFUNewViewControllerDelegatge>
#else
<SFURenewalViewControllerDelegatge>
#endif

@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *roomTF;
@property (weak, nonatomic) IBOutlet UIButton *joinBtn;
@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _joinBtn.layer.cornerRadius = 10;
    _joinBtn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// MARK: - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

// MARK: - Actions
- (IBAction)onTapped:(id)sender {
    [self textFieldShouldReturn:_nameTF];
    [self textFieldShouldReturn:_roomTF];
}

- (IBAction)onJoin:(id)sender {
#if PORTRAIT_MODE
    SFUNewViewController* dest = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SFUNewViewController"];
#else
    SFURenewalViewController* dest = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SFURenewalViewController"];;
#endif
    dest.userName = [_nameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    dest.roomName = [_roomTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    dest.delegate = self;
    
    [self presentViewController:dest animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"joinroom"]) {
#if PORTRAIT_MODE
        SFUNewViewController* dest = [[SFUNewViewController alloc] init];;
#else
        SFURenewalViewController* dest = [[SFURenewalViewController alloc] init];;
#endif
        dest.userName = [_nameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        dest.roomName = [_roomTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        dest.delegate = self;
    }
}

// MARK:

- (void) onEndWithConfigure:(NSDictionary*)dictionary {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        SFUCallLog* callLog = [SFUCallLog MR_createEntityInContext:localContext];

        NSDate *callStartTime = dictionary[kCallStartTime];
        NSDate *callEndTime = dictionary[kCallEndTime];
        double callDuration = [dictionary[kCallDuration] doubleValue];
        NSInteger totalAudioSentBytes = [dictionary[kTotalAudioSentBytes] integerValue];
        NSInteger totalVideoSentBytes = [dictionary[kTotalVideoSentBytes] integerValue];
        NSInteger totalAudioRecvBytes = [dictionary[kTotalAudioRecvBytes] integerValue];
        NSInteger totalVideoRecvBytes = [dictionary[kTotalVideoRecvBytes] integerValue];
        
        callLog.callStartTime = callStartTime;
        callLog.callEndTime = callEndTime;
        callLog.callDuration = callDuration;
        callLog.audioSentByte = totalAudioSentBytes;
        callLog.videoSentByte = totalVideoSentBytes;
        callLog.audioRecvByte = totalAudioRecvBytes;
        callLog.videoRecvByte = totalVideoRecvBytes;
    }];
}

@end
