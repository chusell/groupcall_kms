//
//  SFUCallLog+CoreDataClass.h
//  
//
//  Created by aki on 2018. 10. 1..
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface SFUCallLog : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SFUCallLog+CoreDataProperties.h"
