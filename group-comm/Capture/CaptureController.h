//
//  CaptureController.h
//  group-comm
//
//  Created by aki on 2018. 7. 2..
//  Copyright © 2018년 aki. All rights reserved.
//
#include <WebRTC/RTCCameraVideoCapturer.h>
#import <Foundation/Foundation.h>

@interface CaptureController : NSObject

- (instancetype)initWithCapturer:(RTCCameraVideoCapturer *)capturer;

- (void)startCaptureWithWidth:(int)width height:(int)height;

- (void)stopCapture;

- (void)switchCamera;

@end
