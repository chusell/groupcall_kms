//
//  ClientWebSocket.m
//  group-comm
//
//  Created by aki on 2018. 7. 30..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "ClientWebSocket.h"
#import "SRWebSocket.h"
#import "SFUDefs.h"

@interface ClientWebSocket() <SRWebSocketDelegate>

@property (nonatomic, strong) SRWebSocket *socket;

@property (nonatomic) NSInteger msgId;
@property (strong, nonatomic) NSMutableDictionary *retryCounts;
@property (nonatomic) dispatch_queue_t working_queue;

@end

@implementation ClientWebSocket

- (instancetype)init {
    self = [super init];
    if (self) {
        _retryCounts = [NSMutableDictionary dictionary];
        _working_queue = dispatch_queue_create("group-comm.sfu.client.web.socket", DISPATCH_QUEUE_SERIAL);
    }
    return self;
}

- (void) setupWithURL:(NSURL*)url {
    _socket = [[SRWebSocket alloc] initWithURL:url];
    
    _socket.delegate = self;
    [_socket open];
}

- (void) close {
    __weak ClientWebSocket* weakSelf = self;
    dispatch_async(_working_queue, ^{
        __strong ClientWebSocket* strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        [strongSelf.socket close];
    });
}

- (void) sendMessage:(NSDictionary*)message {
    __weak ClientWebSocket* weakSelf = self;
    dispatch_async(_working_queue, ^{
        __strong ClientWebSocket* strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        // 메세지 아이디 생성
        NSString* msgId = [NSString stringWithFormat:@"%@", @(strongSelf.msgId)];
        NSData *messageJSONObject = [NSJSONSerialization dataWithJSONObject:message
                                                                    options:NSJSONWritingPrettyPrinted
                                                                      error:nil];
        NSString *messageString = [[NSString alloc] initWithData:messageJSONObject encoding:NSUTF8StringEncoding];
        [strongSelf.retryCounts setValue:@(0) forKey:msgId];
        
        strongSelf.msgId++;
        
        NSLogD(@"[ClientWebSocket] send Message. [msg %@]", msgId);
        [strongSelf sendMessage_queue:messageString withMsgId:msgId];
    });
}

- (void) sendMessage_queue:(NSString*)message withMsgId:(NSString*)msgId {
    if (!self.isOpenedWs) {
        // 아직 오픈되어 있지 않으면 retry
        int retryCount = [_retryCounts[msgId] intValue];
        if ( retryCount > 10) {
            // 200ms * 10 회 만큼 재시도함. 종료함
            NSLogE(@"[ClientWebSocket] try to send Message. but socket it not opend yet. [msg %@] retry count is over 10 times. close", msgId);
            
            // delegate 로 close를 전달하여 추후에 새로 설정할수 있도록 수정
            __weak ClientWebSocket* weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong ClientWebSocket* strongSelf = weakSelf;
                if (!strongSelf) {
                    return;
                }
                
                NSError *error = [NSError errorWithDomain:SFUErrorDomain
                                                     code:SFUErrorConnectionFailed
                                                 userInfo:nil];
                [strongSelf.delegate failedWithError:error];
            });
        } else {
            NSLogD(@"[ClientWebSocket] try to send Message. but socket it not opend yet. [msg %@] retry (%@)", msgId, @(retryCount));
            
            retryCount++;
            [_retryCounts setValue:@(retryCount) forKey:msgId];
            
            __weak ClientWebSocket* weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), _working_queue, ^{
                __strong ClientWebSocket* strongSelf = weakSelf;
                if (!strongSelf) {
                    return;
                }
                
                [strongSelf sendMessage_queue:message withMsgId:msgId];
            });
        }
        return;
    }
    
    // 전송
//    NSLogD(@"[ClientWebSocket] Client send (msg id : %@)", msgId);
    NSLogD(@"[ClientWebSocket] Client send : %@", message);
    [_socket send:message];
}

- (BOOL) isOpenedWs {
    return _socket? (_socket.readyState == SR_OPEN) : NO;
}

// MARK: - SRWebSocketDelegate

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLogI(@"[ClientWebSocket] WebSocket connection opened.");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSString *messageString = message;
    NSData *messageData = [messageString dataUsingEncoding:NSUTF8StringEncoding];
    NSJSONSerialization *jsonObject = [NSJSONSerialization JSONObjectWithData:messageData
                                                                      options:0
                                                                        error:nil];
    
    if (![jsonObject isKindOfClass:[NSDictionary class]]) {
        NSLogE(@"Unexpected message: %@", jsonObject);
        return;
    }
    
    
    NSDictionary *wssMessage = (NSDictionary*)jsonObject;
    NSLogD(@"[ClientWebSocket] WebSocket recv : %@", wssMessage);
    
    __weak ClientWebSocket* weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong ClientWebSocket* strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        // client 로 전달
        [strongSelf.delegate processSignalMsg:wssMessage];
    });
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLogD(@"[ClientWebSocket] WebSocket error: %@", error);
}

- (void)webSocket:(SRWebSocket *)webSocket
 didCloseWithCode:(NSInteger)code
           reason:(NSString *)reason
         wasClean:(BOOL)wasClean {
    
    NSLogI(@"[ClientWebSocket] WebSocket closed with code: %ld reason:%@ wasClean:%d", (long)code, reason, wasClean);
    
    // delegate 로 close를 전달하여 추후에 새로 설정할수 있도록 수정
    __weak ClientWebSocket* weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong ClientWebSocket* strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        // client 로 전달
        [strongSelf.delegate socketClosed];
    });
    
}

@end
