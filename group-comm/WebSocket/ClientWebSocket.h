//
//  ClientWebSocket.h
//  group-comm
//
//  Created by aki on 2018. 7. 30..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClientWebSocketDelegate

- (void) processSignalMsg:(NSDictionary*) msg;
- (void) socketClosed;
- (void) failedWithError:(NSError*)error;

@end



// MARK: -

@interface ClientWebSocket : NSObject

@property (weak, nonatomic) id<ClientWebSocketDelegate> delegate;
@property (nonatomic, readonly) BOOL isOpenedWs;

- (void) setupWithURL:(NSURL*)url;
- (void) close;

- (void) sendMessage:(NSDictionary*)message;

@end
