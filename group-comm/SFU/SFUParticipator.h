//
//  SFUParticipator.h
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFUEAGLVideoView.h"
#import "ARDStatsBuilder.h"

@class SFUParticipator;

@protocol SFUParticipatorDelegate <NSObject>

- (void) participator:(SFUParticipator*)participator onChangedSize:(CGSize)size;

@end

@interface SFUParticipator : NSObject

@property (nonatomic, strong, readonly) NSString* name;
@property (nonatomic, strong, readonly) RTCVideoTrack* track;
@property (nonatomic, strong, readonly) SFUEAGLVideoView* view;
@property (nonatomic, strong, readonly) ARDStatsBuilder *statsBuilder;
@property (nonatomic, readonly) CGSize size;

@property (nonatomic, weak) id<SFUParticipatorDelegate> delegate;

- (instancetype)initWithName:(NSString*)name track:(RTCVideoTrack*)track;

@end
