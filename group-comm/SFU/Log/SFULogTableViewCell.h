//
//  SFULogTableViewCell.h
//  group-comm
//
//  Created by aki on 2018. 9. 17..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFUCallLog+CoreDataProperties.h"

@interface SFULogTableViewCell : UITableViewCell
- (void) configureCallLog:(SFUCallLog*)callLog;
@end
