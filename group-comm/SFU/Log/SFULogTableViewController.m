//
//  SFULogTableViewController.m
//  group-comm
//
//  Created by aki on 2018. 9. 17..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFULogTableViewController.h"
#import "SFULogTableViewCell.h"

#import "MagicalRecord/MagicalRecord.h"
#import "SFUCallLog+CoreDataProperties.h"

#define CELL_ID @"CELL_ID"

@interface SFULogTableViewController () <UITableViewDelegate, UITableViewDataSource /*, NSFetchedResultsControllerDelegate*/>
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SFULogTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
#if PORTRAIT_MODE
    [_tableView registerNib:[UINib nibWithNibName:@"SFULogTableViewCell_Port" bundle:nil] forCellReuseIdentifier:CELL_ID];
#else
    [_tableView registerNib:[UINib nibWithNibName:@"SFULogTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_ID];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[SFUCallLog MR_findAll] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SFULogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    
    SFUCallLog *callLog = [[SFUCallLog MR_findAll] objectAtIndex:indexPath.row];
    
    [cell configureCallLog:callLog];
    return cell;
}

// MARK: -
- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onReset:(id)sender {
    __weak SFULogTableViewController* weakSelf = self;
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"통화기록 초기화"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* confirm = [UIAlertAction
                              actionWithTitle:@"확인"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action) {
                                  [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                                      [SFUCallLog MR_truncateAllInContext:localContext];
                                  } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                                      __strong SFULogTableViewController* strongSelf = weakSelf;
                                      if (!strongSelf) {
                                          return;
                                      }
                                      [strongSelf.tableView reloadData];
                                  }];
                              }];
    UIAlertAction* cancel = [UIAlertAction
                              actionWithTitle:@"취소"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action) {
                                  
                              }];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
