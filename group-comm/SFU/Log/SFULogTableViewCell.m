//
//  SFULogTableViewCell.m
//  group-comm
//
//  Created by aki on 2018. 9. 17..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFULogTableViewCell.h"

@interface SFULogTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *callDuration;
@property (weak, nonatomic) IBOutlet UILabel *audioSent;
@property (weak, nonatomic) IBOutlet UILabel *audioRecv;
@property (weak, nonatomic) IBOutlet UILabel *videoSent;
@property (weak, nonatomic) IBOutlet UILabel *videoRecv;
@property (weak, nonatomic) IBOutlet UILabel *total;
@end

@implementation SFULogTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureCallLog:(SFUCallLog*)callLog {
    double duration = callLog.callDuration;
    NSInteger total = callLog.audioSentByte + callLog.videoSentByte + callLog.audioRecvByte + callLog.videoRecvByte;
    
    float audioSentBps = (float)(callLog.audioSentByte * 8) / duration;
    float audioRecvBps = (float)(callLog.audioRecvByte * 8) / duration;
    float videoSentBps = (float)(callLog.videoSentByte * 8) / duration;
    float videoRecvBps = (float)(callLog.videoRecvByte * 8) / duration;
    float totalBps = (float)(total * 8) / duration;
    
    // M, K로 변환
    NSString* strAudioSentByte = [NSString stringWithFormat:@"%@B", [self convertData:callLog.audioSentByte]];
    NSString* strAudioRecvByte = [NSString stringWithFormat:@"%@B", [self convertData:callLog.audioRecvByte]];
    NSString* strVideoSentByte = [NSString stringWithFormat:@"%@B", [self convertData:callLog.videoSentByte]];
    NSString* strVideoRecvByte = [NSString stringWithFormat:@"%@B", [self convertData:callLog.videoRecvByte]];
    NSString* strTotalByte = [NSString stringWithFormat:@"%@B", [self convertData:total]];
    
    NSString* strAudioSentBps = [NSString stringWithFormat:@"%@bps", [self convertData:audioSentBps]];
    NSString* strAudioRecvBps = [NSString stringWithFormat:@"%@bps", [self convertData:audioRecvBps]];
    NSString* strVideoSentBps = [NSString stringWithFormat:@"%@bps", [self convertData:videoSentBps]];
    NSString* strVideoRecvBps = [NSString stringWithFormat:@"%@bps", [self convertData:videoRecvBps]];
    NSString* strTotalBps = [NSString stringWithFormat:@"%@bps", [self convertData:totalBps]];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM-dd HH:mm:ss"];
    _callDuration.text = [NSString stringWithFormat:@"%@  ~  %@\n(%.1f 초)", [format stringFromDate:callLog.callStartTime], [format stringFromDate:callLog.callEndTime], callLog.callDuration];
    
    _audioSent.text = [NSString stringWithFormat:@"%@ (%@)", strAudioSentByte, strAudioSentBps];
    _audioRecv.text = [NSString stringWithFormat:@"%@ (%@)", strAudioRecvByte, strAudioRecvBps];
    _videoSent.text = [NSString stringWithFormat:@"%@ (%@)", strVideoSentByte, strVideoSentBps];
    _videoRecv.text = [NSString stringWithFormat:@"%@ (%@)", strVideoRecvByte, strVideoRecvBps];
    _total.text = [NSString stringWithFormat:@"%@ (%@)", strTotalByte, strTotalBps];
}

- (NSString *)convertData:(float)data {
    if (data > 1e6) {
        return [NSString stringWithFormat:@"%.2f M", data * 1e-6];
    } else if (data > 1e3) {
        return [NSString stringWithFormat:@"%.0f K", data * 1e-3];
    } else {
        return [NSString stringWithFormat:@"%.0f ", data];
    }
}
@end
