//
//  SFUParticipator.m
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFUParticipator.h"
#import "SFUDefs.h"

@interface SFUParticipator() <RTCEAGLVideoViewDelegate>

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) RTCVideoTrack* track;
@property (nonatomic, strong) SFUEAGLVideoView* view;
@property (nonatomic, strong) ARDStatsBuilder *statsBuilder;
@property (nonatomic) CGSize size;

@end

@implementation SFUParticipator

- (instancetype)initWithName:(NSString*)name track:(RTCVideoTrack*)track {
    self = [super init];
    if (self) {
        _name = name;
        _track = track;
        
        // view
        _view = [[SFUEAGLVideoView alloc] initWithFrame:CGRectZero];
        _view.delegate = self;
        
        // 히든 id 부분 제거하고 보여지게 하라
        NSString* displayName = name;
        NSInteger hiddenIdValue = 0;
        NSRange range = [name rangeOfString:HIDDEN_ID];
        if (range.location != NSNotFound) {
            displayName = [name substringToIndex:range.location];
            
            NSString* hiddenId = [[name substringFromIndex:(range.location + HIDDEN_ID.length)] substringToIndex:1];
            hiddenIdValue = [hiddenId integerValue];
        }
        _view.nameLabel.text = displayName;
        
        // set image type
        [_view setImageType:hiddenIdValue];
        
        // link track -> view
        [_track addRenderer:_view];
        
        _statsBuilder = [[ARDStatsBuilder alloc] init];
    }
    return self;
}

- (NSString *)description {
    NSString* desc = [NSString stringWithFormat:@"<SFUParticipator:%p> name: %@ , view: <SFUEAGLVideoView:%p>", self, _name, _view];
    
    return desc;
}

- (void)videoView:(id<RTCVideoRenderer>)videoView didChangeVideoSize:(CGSize)size {
    if (videoView == _view) {
        _size = size;
        
        if (_delegate) {
            [_delegate participator:self onChangedSize:size];
        }
    }
}

@end
