//
//  SFUEAGLVideoView.h
//  group-comm
//
//  Created by aki on 2018. 8. 29..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <WebRTC/WebRTC.h>

@interface SFUEAGLVideoView : RTCEAGLVideoView

@property (strong, nonatomic, readonly) UILabel* nameLabel;
@property (nonatomic, readonly) double lastCoverOpenedTimeMs;
@property (nonatomic, strong, readonly) UIView *coverView;

- (void)reset;
- (void)setImageType:(NSInteger)imageType;

@end
