//
//  SFUDefs.h
//  group-comm
//
//  Created by aki on 2018. 9. 19..
//  Copyright © 2018년 aki. All rights reserved.
//

#ifndef SFUDefs_h
#define SFUDefs_h

#define ROOM_NAME @"12345"

#define REMOTEVIEWS_CELL_ID @"remote_views"
#define FIND_LONGEST_SPEAKER_INTERVAL 4 // 3 -> 4

#define HIDDEN_ID @":id:"

#if PORTRAIT_MODE
#define CAMERA_WIDTH 480
#define CAMERA_HEIGHT 640
#else
#define CAMERA_WIDTH 640
#define CAMERA_HEIGHT 480
#endif

#define CELL_PORT_WIDTH 140         // 120x160
#define CELL_PORT_MAIN_WIDTH 180    // 160x213, 200x267, 

#define CELL_LAND_WIDTH 160
#define CELL_LAND_MAIN_WIDTH 200

#define SFUErrorDomain @"SFUError"

#define kCallStartTime @"callStartTime"
#define kCallEndTime @"callEndTime"
#define kCallDuration @"callDuration"
#define kTotalAudioSentBytes @"totalAudioSentBytes"
#define kTotalVideoSentBytes @"totalVideoSentBytes"
#define kTotalAudioRecvBytes @"totalAudioRecvBytes"
#define kTotalVideoRecvBytes @"totalVideoRecvBytes"

typedef NS_ENUM(NSInteger, SFUError) {
    SFUErrorUnknown = 0,
    SFUErrorConnectionFailed,
    
} NS_ENUM_AVAILABLE(10_10, 8_0);



#endif /* SFUDefs_h */
