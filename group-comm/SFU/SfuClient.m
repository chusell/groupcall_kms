//
//  SfuClient.m
//  group-comm
//
//  Created by aki on 2018. 7. 30..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SfuClient.h"
#import "SFUDefs.h"

// host address
//#define WS_HOST @"wss://172.23.147:8443/groupcall"
#define WS_HOST @"wss://1.232.90.69:8443/groupcall"
#define WS_BACKOFF_HOST @"wss://192.168.1.120:8443/groupcall"

typedef NS_ENUM(NSUInteger, SfuDirection) {
    SfuDirectionSendOnly,
    SfuDirectionRecvOnly,
};


// MARK: -
@interface SfuTimerProxy : NSObject

- (instancetype)initWithInterval:(NSTimeInterval)interval
                         repeats:(BOOL)repeats
                    timerHandler:(void (^)(void))timerHandler;
- (void)invalidate;

@end

@implementation SfuTimerProxy {
    NSTimer *_timer;
    void (^_timerHandler)(void);
}

- (instancetype)initWithInterval:(NSTimeInterval)interval
                         repeats:(BOOL)repeats
                    timerHandler:(void (^)(void))timerHandler {
    NSParameterAssert(timerHandler);
    if (self = [super init]) {
        _timerHandler = timerHandler;
        _timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                  target:self
                                                selector:@selector(timerDidFire:)
                                                userInfo:nil
                                                 repeats:repeats];
    }
    return self;
}

- (void)invalidate {
    [_timer invalidate];
}

- (void)timerDidFire:(NSTimer *)timer {
    _timerHandler();
}

@end


// MARK: -
@interface SfuClient () <RTCPeerConnectionDelegate, RTCAudioSessionDelegate, ClientWebSocketDelegate, SpeechDetectorDelegate>

@property (strong, nonatomic) ClientWebSocket* ws;

@property (strong, nonatomic) NSMutableDictionary<NSString*, RTCPeerConnection*> *peerConnections;
@property (strong, nonatomic) RTCPeerConnectionFactory *peerConnectionFactory;

@property (strong, nonatomic) RTCVideoTrack *localVideoTrack;

@property (strong, nonatomic) NSString* userName;
@property (strong, nonatomic) NSString* room;

@property (strong, nonatomic) SfuTimerProxy* statsTimer;
@property (nonatomic) BOOL shouldGetStats;

@property (nonatomic) BOOL tryBackoff;

@end


// MARK: -
@implementation SfuClient

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    _peerConnections = [NSMutableDictionary dictionary];
    
    // webrtc log level
    RTCSetMinDebugLogLevel(RTCLoggingSeverityNone);
    
    _tryBackoff = NO;
}

// MARK: - Public API

- (void)joinRoom:(NSString*)room userName:(NSString*)name {
    [self setupWs];
    
    // send join message
    _userName = name;
    _room = room;
    NSDictionary* msg = @{ @"id" : @"joinRoom",
                           @"name" : name,
                           @"room" : room };
    [_ws sendMessage:msg];
    
    self.shouldGetStats = YES;
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)leaveRoom {
    // send leave message
    NSDictionary* msg = @{ @"id" : @"leaveRoom" };
    [_ws sendMessage:msg];
    
    // clear resources
    [self removeAllPeerConnection];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    self.shouldGetStats = NO;
    
    [self closeWs];
}

// MARK: - Configure PeerConnection

- (void)setupAudioConfiguration {
    RTCAudioSessionConfiguration *webRTCConfig = [RTCAudioSessionConfiguration webRTCConfiguration];
    webRTCConfig.categoryOptions = webRTCConfig.categoryOptions | AVAudioSessionCategoryOptionDefaultToSpeaker;
    [RTCAudioSessionConfiguration setWebRTCConfiguration:webRTCConfig];
}

- (void)createPeerconnectionFactory {
    [self setupAudioConfiguration];
    _peerConnectionFactory = [[RTCPeerConnectionFactory alloc] init];
}

- (void)createPeerConnectionWithDirection:(SfuDirection)direction participantName:(NSString*)participant {
    RTCConfiguration *config = [[RTCConfiguration alloc] init];
    config.iceServers = @[
                          [[RTCIceServer alloc] initWithURLStrings:@[ @"stun:stun.stunprotocol.org:3478" ]
                                                           username:nil
                                                         credential:nil],
//                           [[RTCIceServer alloc] initWithURLStrings:@[ @"stun:stun3.l.google.com:19302" ]
//                                                           username:nil
//                                                         credential:nil]
                           ];
    config.sdpSemantics = RTCSdpSemanticsUnifiedPlan;
    RTCMediaConstraints *constraints = [self defaultPeerConnectionConstraints];
    
    // peer connection 생성
    RTCPeerConnection *peerConnection = [_peerConnectionFactory peerConnectionWithConfiguration:config
                                                                                    constraints:constraints
                                                                                       delegate:self];
    
    // 유저리스트에 추가
    [_peerConnections setObject:peerConnection forKey:participant];
    
    if (direction == SfuDirectionSendOnly) {
        // sender 생성
        [self createMediaSenders:peerConnection];
    }
    
    __weak SfuClient* weakSelf = self;
    [peerConnection offerForConstraints:[self defaultMediaConstraintsWithSendOnly:(direction == SfuDirectionSendOnly)]
                      completionHandler:^(RTCSessionDescription * _Nullable sdp, NSError * _Nullable error) {
                          __strong SfuClient* strongSelf = weakSelf;
                          if (!strongSelf) {
                              return;
                          }
                          
                          // set local description
                          RTCPeerConnection* pc = strongSelf.peerConnections[participant];
                          [pc setLocalDescription:sdp
                                completionHandler:^(NSError * _Nullable error) {}];
                          
                          // 서버로 전송
                          NSLogD(@"%@ -> send SdpOffer : %@", participant, (direction == SfuDirectionSendOnly)? @"sendonly" : @"recvonly");
                          NSDictionary* msg = @{ @"id" : @"receiveVideoFrom",
                                                 @"sdpOffer" : sdp.sdp,
                                                 @"sender" : participant };
                          [strongSelf.ws sendMessage:msg];
                      }];
    
    NSLogI(@"PeerConnection (%@) created", participant);
}

- (void)removeAllPeerConnection {
    // 앱에 종료를 알림
    [_delegate clientIsDisconnected:self];
    
    // outoing track 삭제
    _localVideoTrack = nil;;
    
    // 기존에 존재했던 peerconnection 모두 삭제
    [self participantForEachWithClosure:^(RTCPeerConnection *peerConnection) {
        [peerConnection close];
    }];
    [_peerConnections removeAllObjects];
    
    _peerConnectionFactory = nil;
}

// MARK: - PeerConnection creatation utils

- (RTCMediaConstraints *)defaultPeerConnectionConstraints {
    NSDictionary *optionalConstraints = @{ @"DtlsSrtpKeyAgreement" : kRTCMediaConstraintsValueTrue };
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil
                                                                             optionalConstraints:optionalConstraints];
    return constraints;
}

- (RTCMediaConstraints *)defaultMediaConstraintsWithSendOnly:(BOOL)sendOnly {
    NSDictionary *mandatoryConstraints = @{ kRTCMediaConstraintsOfferToReceiveAudio : sendOnly? kRTCMediaConstraintsValueFalse : kRTCMediaConstraintsValueTrue,
                                            kRTCMediaConstraintsOfferToReceiveVideo : sendOnly? kRTCMediaConstraintsValueFalse : kRTCMediaConstraintsValueTrue };
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints
                                                                             optionalConstraints:nil];
    return constraints;
}

- (void)createMediaSenders:(RTCPeerConnection*)peerconnection {
    // audio tx track
    RTCMediaConstraints *constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil
                                                                             optionalConstraints:nil];
    RTCAudioSource *source = [_peerConnectionFactory audioSourceWithConstraints:constraints];
    RTCAudioTrack *track = [_peerConnectionFactory audioTrackWithSource:source
                                                                trackId:@"ARDAMSa0"];
    [peerconnection addTrack:track streamIds:@[ @"ARDAMS" ]];
    
    // video tx track
    if (!_localVideoTrack) {
        _localVideoTrack = [self createLocalVideoTrack];
    }
    [peerconnection addTrack:_localVideoTrack streamIds:@[ @"ARDAMS" ]];
}

- (RTCVideoTrack *)createLocalVideoTrack {
    RTCVideoSource *source = [_peerConnectionFactory videoSource];
    RTCCameraVideoCapturer *capturer = [[RTCCameraVideoCapturer alloc] initWithDelegate:source];
    
    [_delegate client:self didCreateLocalCapturer:capturer];
    
    // factory에 speech detector delegate 등록
    _peerConnectionFactory.speechDetectorDelegate = self;
    
    return [_peerConnectionFactory videoTrackWithSource:source trackId:@"ARDAMSv0"];
}

- (void)createVideoReceiver:(RTCPeerConnection*)peerconnection name:(NSString*)name {
    // We can set up rendering for the remote track right away since the transceiver already has an
    // RTCRtpReceiver with a track. The track will automatically get unmuted and produce frames
    // once RTP is received.
    
    RTCRtpTransceiver *transeiver = [self videoTransceiverFromPeerConnection:peerconnection];
    RTCVideoTrack *track = (RTCVideoTrack *)(transeiver.receiver.track);
    if (!track) {
        [NSException raise:@"SFU" format:@"track is nil"];
        return;
    }
    
    __weak SfuClient *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong SfuClient *strongSelf = weakSelf;
         [strongSelf.delegate client:strongSelf didReceiveRemoteVideoTrack:track name:name];
    });
}

- (RTCRtpTransceiver *)videoTransceiverFromPeerConnection:(RTCPeerConnection*)peerconnection {
    for (RTCRtpTransceiver *transceiver in peerconnection.transceivers) {
        if (transceiver.mediaType == RTCRtpMediaTypeVideo) {
            return transceiver;
        }
    }
    return nil;
}

// MARK: - Privates

- (void)participantForEachWithClosure:(void (^)(RTCPeerConnection *peerConnection))closure{
    if (!closure) {
        return;
    }
    
    NSArray<NSString*> *keys = [_peerConnections allKeys];
    for (NSString* key in keys) {
        RTCPeerConnection* peerconnection = _peerConnections[key];
        if (peerconnection) {
            closure(peerconnection);
        }
    }
}

// MARK: - RTCPeerConnectionDelegate

/** Called when the SignalingState changed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeSignalingState:(RTCSignalingState)stateChanged {
    NSLogD(@"[didChangeSignalingState]");
}

/** Called when media is received on a new stream from remote peer. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
          didAddStream:(RTCMediaStream *)stream {
    NSLogD(@"[didAddStream]");
}

/** Called when a remote peer closes a stream. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
       didRemoveStream:(RTCMediaStream *)stream {
    NSLogD(@"[didRemoveStream]");
}

/** Called when negotiation is needed, for example ICE has restarted. */
- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection {
    NSLogD(@"[peerConnectionShouldNegotiate]");
}

/** Called any time the IceConnectionState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeIceConnectionState:(RTCIceConnectionState)newState {
    NSLogD(@"[didChangeIceConnectionState]");
}

/** Called any time the IceGatheringState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeIceGatheringState:(RTCIceGatheringState)newState {
    NSLogD(@"[didChangeIceGatheringState]");
}

/** New ice candidate has been found. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didGenerateIceCandidate:(RTCIceCandidate *)candidate {
    // 여기서 서버로 candidate 정보를 보내야함
    NSArray<NSString*> *names = [_peerConnections allKeys];
    for (NSString* name in names) {
        RTCPeerConnection* peerconnection = _peerConnections[name];
        if (peerconnection == peerConnection) {
            NSDictionary* msg = @{ @"id" : @"onIceCandidate",
                                   @"name" : name,
                                   @"candidate": @{ @"candidate" : candidate.sdp,
                                                    @"sdpMid" : candidate.sdpMid,
                                                    @"sdpMLineIndex" : @(candidate.sdpMLineIndex) }
                                   };
            [_ws sendMessage:msg];
        }
    }
}

/** Called when a group of local Ice candidates have been removed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didRemoveIceCandidates:(NSArray<RTCIceCandidate *> *)candidates {
    NSLogD(@"[didRemoveIceCandidates]");
}

/** New data channel has been opened. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didOpenDataChannel:(RTCDataChannel *)dataChannel {
    NSLogD(@"[didOpenDataChannel]");
}

// MARK: - SpeechDetectorDelegate
- (void)onChangeActiveSpeech:(BOOL)active {
    // speech detector로부터 speech active 변경을 전달받음 -> 서버로 메세지 전송
//    NSLogI(@"[speech detector] onChangeActiveSpeech %@", @(active));
    if (active) {
        // requestAllowSendingVideo 메세지 전달
        NSDictionary* msg = @{ @"id" : @"requestAllowSendingVideo",
                               @"priority" : @1 };
        [_ws sendMessage:msg];
    } else {
        // StoppingVideo 메세지 전달
        NSDictionary* msg = @{ @"id" : @"stoppingVideo" };
        [_ws sendMessage:msg];
    }
    
    // 앱에 전달
    __weak SfuClient* weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong SfuClient* strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(client:onActiveSpeech:)]) {
            [strongSelf.delegate client:strongSelf onActiveSpeech:active];
        }
    });
}

// MARK: - WebSocket

- (void)setupWs {
    [self closeWs];
    
    _ws = [[ClientWebSocket alloc] init];
    _ws.delegate = self;
    
    NSString* url = (_tryBackoff == NO)? WS_HOST : WS_BACKOFF_HOST;
    [_ws setupWithURL:[NSURL URLWithString:url]];
}

- (void)closeWs {
    if (_ws) {
        [_ws close];
        
        _ws.delegate = nil;
        _ws = nil;
    }
}

// MARK: - WebSocket Delegate

// received msg
- (void)processSignalMsg:(NSDictionary *)msg {
    NSString* strId = msg[@"id"];
    NSLogD(@"[recv msg] %@ : %@", strId, msg);
    
    if ([strId isEqualToString:@"existingParticipants"]) {
        // 최초 내가 접속했을때 오는 1회성 메세지
        // peer connection factory 생성
        [self createPeerconnectionFactory];
        
        // send only로 peer connection 생성, 미디어 서버로 전송
        NSString* name = _userName;
        [self createPeerConnectionWithDirection:SfuDirectionSendOnly
                                participantName:name];
        
        // 기존에 접속해있던 유저가 있었으면
        // 그 유저들에 대해 recvonly peerconnection 을 생성하라
        NSArray<NSString*> *users = msg[@"data"];
        for (NSString* user in users) {
            // recvonly peerConnection 생성, 미디어 서버로 전송
            [self createPeerConnectionWithDirection:SfuDirectionRecvOnly
                                    participantName:user];
        }
    } else if ([strId isEqualToString:@"newParticipantArrived"]) {
        // 새로운 참가자가 입장함
        NSString* name = msg[@"name"];
        
        // 새로운 참가자에 대한 recvonly peerconnection 을 생성하라
        [self createPeerConnectionWithDirection:SfuDirectionRecvOnly
                                participantName:name];
        
    } else if ([strId isEqualToString:@"participantLeft"]) {
        // 참가자가 나감
        NSString* name = msg[@"name"];
        
        // 앱에 먼저 알려서 뷰를 삭제
        __weak SfuClient *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong SfuClient *strongSelf = weakSelf;
            [strongSelf.delegate client:strongSelf didReceiveRemoteVideoTrack:nil name:name];
        });
        
        // 뷰가 사라졌으니 peerConnection 삭제
        RTCPeerConnection* peerConnection = _peerConnections[name];
        [peerConnection close];
        peerConnection = nil;
        
        [_peerConnections removeObjectForKey:name];

    } else if ([strId isEqualToString:@"receiveVideoAnswer"]) {
        // name 유저의 응답이 옴
        NSString* name = msg[@"name"];
        RTCPeerConnection* peerConnection = _peerConnections[name];
        if (!peerConnection) {
            // ERROR
            [NSException raise:@"SFU" format:@"peer connection is nil"];
            return;
        }
        
        NSString* sdpString = msg[@"sdpAnswer"];
        RTCSessionDescription* answerSdp = [[RTCSessionDescription alloc] initWithType:RTCSdpTypeAnswer sdp:sdpString];
        [peerConnection setRemoteDescription:answerSdp
                           completionHandler:^(NSError * _Nullable error) {}];
        
        
        if (![name isEqualToString:_userName]) {
            // username의 peerconnection은 sendonly 이므로 receiver를 생성할필요가 없다
            [self createVideoReceiver:peerConnection
                                 name:name];
        }
        
    } else if ([strId isEqualToString:@"iceCandidate"]) {
        // name 유저의 iceCandidate를 추가하라
        NSString* name = msg[@"name"];
        
        NSDictionary* candidateMsg = msg[@"candidate"];
        NSString* iceSdp = candidateMsg[@"candidate"];
        NSNumber* iceSdpMLineIndex = candidateMsg[@"sdpMLineIndex"];
        NSString* iceSdpMid = candidateMsg[@"sdpMid"];
        
        NSString* replacement = [iceSdp stringByReplacingOccurrencesOfString:@"192.168.1.120" withString:@"1.232.90.69"];
        NSLogD(@"ip address of kms changed // %@  ->  %@", iceSdp, replacement);
        
        RTCIceCandidate* iceCandidate = [[RTCIceCandidate alloc] initWithSdp:replacement
                                                               sdpMLineIndex:[iceSdpMLineIndex intValue]
                                                                      sdpMid:iceSdpMid];
        
        RTCPeerConnection* peerConnection = _peerConnections[name];
        if (!peerConnection) {
            // ERROR
            [NSException raise:@"SFU" format:@"peer connection is nil"];
            return;
        }
        [peerConnection addIceCandidate:iceCandidate];
        
    } else if ([strId isEqualToString:@"videoAllow"]) {
        //서버로부터 allow, disallow 메세지를 수신함
        // 이것을 다시 speech detector에게 전달함
        BOOL allow = [msg[@"value"] boolValue];
//        NSLogI(@"[speech detector]      -> recv videoAllow %@", @(allow));
        
        [_peerConnectionFactory speechDetectorMessageWithAllow:allow];
        
        // 앱에 전달
        __weak SfuClient* weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong SfuClient* strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(client:didRecvAllowSendingVideo:)]) {
                [strongSelf.delegate client:strongSelf didRecvAllowSendingVideo:allow];
            }
        });
    } else {
        NSLogE(@"ERROR. Unrecognized message : %@ :%@", strId, msg);
    }
}

- (void) socketClosed {
    [self closeWs];
}

- (void) failedWithError:(NSError*)error {
    if (error) {
        if (_tryBackoff == NO) {
            // backoff try
            _tryBackoff = YES;
            [self joinRoom:_room userName:_userName];
        } else {
            NSLogE(@"[SFUClient] failedWithError : %@", error);
            [self.delegate client:self onError:error];
        }
    }
}

// MARK: - Stats
- (void) setShouldGetStats:(BOOL)shouldGetStats {
    if (_shouldGetStats == shouldGetStats) {
        return;
    }
    if (shouldGetStats) {
        __weak SfuClient *weakSelf = self;
        _statsTimer = [[SfuTimerProxy alloc] initWithInterval:1
                                                      repeats:YES
                                                 timerHandler:^{
                                                     __strong SfuClient *strongSelf = weakSelf;
                           
                                                     NSArray<NSString*>* keys = [strongSelf.peerConnections allKeys];
                                                     for (NSString* key in keys) {
                                                         RTCPeerConnection *peerConnection = strongSelf.peerConnections[key];
                                                         
                                                         BOOL isSendonly = [key isEqualToString:strongSelf.userName];
                                                         [strongSelf callStats:peerConnection
                                                                          name:key
                                                                      sendonly:isSendonly];
                                                     }
                                                 }];
    } else {
        [_statsTimer invalidate];
        _statsTimer = nil;
    }
    _shouldGetStats = shouldGetStats;
}

- (void) callStats:(RTCPeerConnection *)peerConnection name:(NSString*)name sendonly:(BOOL)sendonly {
    __weak SfuClient *weakSelf = self;
    [peerConnection statsForTrack:nil
                 statsOutputLevel:RTCStatsOutputLevelDebug
                completionHandler:^(NSArray *stats) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        __strong SfuClient *strongSelf = weakSelf;
                        
                        [strongSelf.delegate client:strongSelf
                                        didGetStats:stats
                                               name:name
                                           sendonly:sendonly];
                    });
                }];
}

@end
