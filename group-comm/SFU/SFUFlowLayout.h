//
//  SFUFlowLayout.h
//  group-comm
//
//  Created by aki on 2018. 10. 11..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SFUFlowLayout : UICollectionViewFlowLayout

@end

NS_ASSUME_NONNULL_END
