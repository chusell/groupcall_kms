//
//  SFUEAGLVideoView.m
//  group-comm
//
//  Created by aki on 2018. 8. 29..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFUEAGLVideoView.h"

@interface SFUEAGLVideoView()
@property (strong, nonatomic) UILabel* nameLabel;

@property (nonatomic, strong) UIImageView *coverView;
@property (nonatomic) double lastRenderedTimeMs;
@property (nonatomic) double lastCoverOpenedTimeMs;

@end


@implementation SFUEAGLVideoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView* coverView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_2"]];
        coverView.backgroundColor = [UIColor darkGrayColor];
        coverView.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubview:coverView];
        self.coverView = coverView;
        
        // add label
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Gill Sans" size:0];
        label.backgroundColor = [UIColor colorWithRed:175.0/255.0 green:175.0/255.0 blue:175.0/255.0 alpha:0.4];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        _nameLabel = label;
        
        // initialize
        _lastRenderedTimeMs = 0;
        _lastCoverOpenedTimeMs = DBL_MAX;
        
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    
    // innerview frame
    CGRect bounds = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.coverView.frame = bounds;
    
    CGFloat labelHeight = bounds.size.height/6;
    _nameLabel.font = [UIFont fontWithName:@"Gill Sans" size:fmax(labelHeight-5, 17)];
    _nameLabel.frame = CGRectMake(0, 8, bounds.size.width, labelHeight);
}

- (void)renderStarted:(BOOL)started {
    double now = CFAbsoluteTimeGetCurrent();
    if (started) {
        if (!_coverView.hidden) {
            _coverView.hidden = YES;
            
            // 마지막으로 커버뷰가 오픈된 시간 저장
            // 이걸 활용하여 누가 가장 오랫동안 말하고 있는지를 파악함
            _lastCoverOpenedTimeMs = now;
        }
        
        // 마지막으로 렌더링을 한시간 저장
        _lastRenderedTimeMs = now;
    } else {
        // 300ms 이상 렌더링된것이 없으면
        // 커버뷰로 띄운다
        if ((now - _lastRenderedTimeMs) > 0.3) {
            if (_coverView.hidden) {
                _coverView.hidden = NO;
            }
        }
    }
}

- (void)reset {
    _coverView.hidden = NO;
    _lastRenderedTimeMs = 0;
}

- (void)setImageType:(NSInteger)imageType {
    if (imageType % 2 == 0) {
        _coverView.image = [UIImage imageNamed:@"user_2"];
    } else {
        _coverView.image = [UIImage imageNamed:@"user_2"];
    }
}

@end
