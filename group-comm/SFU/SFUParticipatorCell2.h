//
//  SFUParticipatorCell2.h
//  group-comm
//
//  Created by aki on 2018. 10. 8..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SFUParticipatorCell2 : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *view;

@end

NS_ASSUME_NONNULL_END
