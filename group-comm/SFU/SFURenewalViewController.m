//
//  SFURenewalViewController.m
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFURenewalViewController.h"
#import "StartViewController.h"

#import "SfuClient.h"
#import "CaptureController.h"
#import "SFUParticipator.h"
#import "SFUParticipatorCell.h"
#import "SFUEAGLVideoView.h"
#import "SFUDefs.h"
#import "MagicalRecord/MagicalRecord.h"
#import "SFUCallLog+CoreDataProperties.h"

#import <WebRTC/WebRTC.h>

@interface SFURenewalViewController () <SfuClientDelegate, SFUParticipatorDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) SfuClient* client;

// local
@property (weak, nonatomic) IBOutlet UIView *localViewContainer;
@property (strong, nonatomic) CaptureController* captureController;
@property (strong, nonatomic) RTCCameraPreviewView* localVideoView;
@property (nonatomic, strong) ARDStatsBuilder *localStatsBuilder;

// full-remote
@property (weak, nonatomic) IBOutlet UIView *fullRemoteContainerView;
@property (strong, nonatomic) SFUEAGLVideoView* fullRemoteView;
@property (weak, nonatomic) RTCVideoTrack* fullRemoteTrack;

// remote
@property (weak, nonatomic) IBOutlet UITableView *remoteViews;
@property (strong, nonatomic) NSMutableArray<SFUParticipator*> *participators;

// for call log
@property (nonatomic) NSInteger totalVideoRecvBytes;
@property (nonatomic) NSInteger totalAudioRecvBytes;
@property (strong, nonatomic) NSDate *callStartTime;

// UI
@property (weak, nonatomic) IBOutlet UIButton *leaveBtn;

// local status icons
@property (weak, nonatomic) IBOutlet UIImageView *localSpeakingIcon;
@property (weak, nonatomic) IBOutlet UIImageView *localVideoSendIcon;


// for determine full-remote
@property (nonatomic) BOOL fixedRemoteView;
@property (nonatomic, strong) NSTimer* checkingTimer;

@end

// MARK: -
@implementation SFURenewalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _leaveBtn.layer.cornerRadius = 10;
    _leaveBtn.layer.masksToBounds = YES;
    
    _client = [[SfuClient alloc] init];
    _client.delegate = self;
    _participators = [NSMutableArray array];
    
    _fullRemoteView = [[SFUEAGLVideoView alloc] initWithFrame:CGRectZero];
    [_fullRemoteContainerView addSubview:_fullRemoteView];
    _fullRemoteView.hidden = YES;
    
    _fixedRemoteView = NO;

    _totalVideoRecvBytes = 0;
    _totalAudioRecvBytes = 0;
    
    [_remoteViews registerNib:[UINib nibWithNibName:@"SFUParticipatorCell" bundle:nil] forCellReuseIdentifier:REMOTEVIEWS_CELL_ID];
    
    _localSpeakingIcon.hidden = YES;
    _localVideoSendIcon.hidden = YES;
    
    [self joinRoom];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews {
    if (!_localVideoView) {
        _localVideoView = [[RTCCameraPreviewView alloc] initWithFrame:_localViewContainer.bounds];
        [_localViewContainer addSubview:_localVideoView];
        
        _localViewContainer.layer.cornerRadius = 10;
        _localViewContainer.layer.masksToBounds = YES;
    }
}

- (void)joinRoom {
    NSString* name = _userName;
    if (!_userName || [_userName isEqualToString:@""]) {
        name = [NSString stringWithFormat:@"user%@", @((arc4random() % 1000))];
    }
    
    // unique를 위한 히든 id 추가
    uint32_t rn = (arc4random() % 100000);
    name = [NSString stringWithFormat:@"%@%@%@", name, HIDDEN_ID, @(rn)];

    NSString* room = _roomName;
    if (!_roomName || [_roomName isEqualToString:@""] ) {
        room = ROOM_NAME;
    }
    
    [_client joinRoom:room userName:name];
}

- (IBAction)leaveRoom:(id)sender {
    // TODO: 이 쓰레드와 didReceiveRemoteVideoTrack 실행 쓰레드가 동기화가 되어야함
    // 그래야 _totalAudioRecvBytes, _totalVideoRecvBytes 값이 빠짐이 없다
    // FIXME : 나간사람도 데이터를 그대로 남겨두고 끝나고 한번에 정산하는 방식으로 수정하면 동기화는 필요없을듯
    if (_callStartTime) {
        // 통화시간
        NSDate *callEndTime = [NSDate date];
        NSTimeInterval callDuration = [callEndTime timeIntervalSinceDate:_callStartTime];
        
        // 전송량
        NSInteger totalAudioSentBytes = _localStatsBuilder.audioSendByte;
        NSInteger totalVideoSentBytes = _localStatsBuilder.videoSendByte;
        
        // 현재 접속해있던 유저들의 수신량
        for (SFUParticipator* participator in _participators) {
            _totalAudioRecvBytes += participator.statsBuilder.audioRecvByte;
            _totalVideoRecvBytes += participator.statsBuilder.videoRecvByte;
        }
        
        NSMutableDictionary* dic = [NSMutableDictionary dictionary];
        dic[kCallStartTime] = _callStartTime;
        dic[kCallEndTime] = callEndTime;
        dic[kCallDuration] = [NSNumber numberWithDouble:callDuration];
        dic[kTotalAudioSentBytes] = [NSNumber numberWithInteger:totalAudioSentBytes];
        dic[kTotalVideoSentBytes] = [NSNumber numberWithInteger:totalVideoSentBytes];
        dic[kTotalAudioRecvBytes] = [NSNumber numberWithInteger:_totalAudioRecvBytes];
        dic[kTotalVideoRecvBytes] = [NSNumber numberWithInteger:_totalVideoRecvBytes];
        
        if (_delegate) {
            [_delegate onEndWithConfigure:dic];
        }
        
        // 초기화
        _totalVideoRecvBytes = 0;
        _totalAudioRecvBytes = 0;
        _callStartTime = nil;
    }
    
    // [client leaveRoom] 하고나면 데이터들 다 삭제됨. 미리 다 카피해놔야함
    // room 나가기
    [_client leaveRoom];
    
    [self dismissViewControllerAnimated:YES completion:^{}];
}

// MARK: - SfuClientDelegate

- (void)client:(SfuClient*)client didCreateLocalCapturer:(RTCCameraVideoCapturer *)localCapturer {
    // tx track
    _localVideoView.captureSession = localCapturer.captureSession;
    
    _captureController = [[CaptureController alloc] initWithCapturer:localCapturer];
    [_captureController startCaptureWithWidth:CAMERA_WIDTH height:CAMERA_HEIGHT]; // VGA
    
    _localStatsBuilder = [[ARDStatsBuilder alloc] init];
    
    // 통화 연결 시작됨
    _callStartTime = [NSDate date];
}

- (void)client:(SfuClient*)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack name:(NSString*)name {
    if (remoteVideoTrack) {
        // 새로운 유저 등록
        SFUParticipator* user = [[SFUParticipator alloc] initWithName:name track:remoteVideoTrack];
        user.delegate = self;
        
        [_participators addObject:user];
        
        if (!_checkingTimer) {
            [self createSpeakerCheakingTimer];
            
            // checkingTimer가 없었으면, 첫번째 유저가 접속 한 것이다
            // 최초 타이머 체크하기전에 먼저 디폴트 세팅
            __weak SFURenewalViewController* weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                __strong SFURenewalViewController* strongSelf = weakSelf;
                if (!strongSelf) {
                    return;
                }
                
                [strongSelf selectLongestSpeakingUser];
            });
        }
        
        _remoteViews.hidden = NO;
    } else {
        // 유저 삭제
        for(SFUParticipator* user in _participators) {
            if([user.name isEqualToString:name]) {
                // 유저 삭제하기전에 마지막으로 받은 byte값을 저장해놓음
                // TODO: 이 스레드와 leaveRoom 실행 쓰레드가 동기화가 되어야함
                _totalAudioRecvBytes += user.statsBuilder.audioRecvByte;
                _totalVideoRecvBytes += user.statsBuilder.videoRecvByte;
                
                // 만약 full 유저라면
                if (_fullRemoteTrack == user.track) {
                    [self selectParticipator:nil];
                }
                
                [_participators removeObject:user];
                
                break;
            }
        }
        
        if ([_participators count] == 0) {
            _remoteViews.hidden = YES;
        }
    }
    
    // 데이터가 변경되었으니 리로드
    [_remoteViews reloadData];
}

- (void)clientIsDisconnected:(SfuClient*)client {
    _localVideoView.captureSession = nil;
    [_captureController stopCapture];
    _captureController = nil;
    
    // 연결된 모든정보 삭제
    [_participators removeAllObjects];
    
    // 데이터가 변경되었으니 리로드
    [_remoteViews reloadData];
}

- (void)client:(SfuClient *)client didGetStats:(NSArray *)stats name:(NSString*)name sendonly:(BOOL)sendonly {
    if(sendonly) {
        for (RTCLegacyStatsReport *report in stats) {
            [_localStatsBuilder parseStatsReport:report];
        }

        // bps
        NSInteger audiobps = _localStatsBuilder.audioSendbps * 1e-3;
        NSInteger videobps = _localStatsBuilder.videoSendbps * 1e-3;
        NSLogD(@"[%@] sent : audio (%@ kbps) + video (%@ kbps)  ->  total = %@ kbps", @"LOCAL", @(audiobps), @(videobps), @(audiobps + videobps));
        
    } else {
        for (SFUParticipator* participator in _participators) {
            // SFUParticipator 중에서 동일한 유저를 찾아라
            if ([name isEqualToString:participator.name]) {
                for (RTCLegacyStatsReport *report in stats) {
                    [participator.statsBuilder parseStatsReport:report];
                }
                
                // bps
                NSInteger audiobps = participator.statsBuilder.audioRecvbps * 1e-3;
                NSInteger videobps = participator.statsBuilder.videoRecvbps * 1e-3;
                NSLogD(@"[%@] recv : audio (%@ kbps) + video (%@ kbps)  ->  total = %@ kbps", participator.name, @(audiobps), @(videobps), @(audiobps + videobps));
            }
        }
    }
}

- (void)client:(SfuClient*)client onError:(NSError*)error {
    NSString* errMsg = @"";
    if (error.code == SFUErrorConnectionFailed) {
        errMsg = @"접속 실패";
    } else {
        errMsg = @"Unknown";
    }
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"에러"
                                 message:errMsg
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* confirm = [UIAlertAction
                              actionWithTitle:@"확인"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action) {
                                  [self dismissViewControllerAnimated:YES completion:nil];
                              }];
    [alert addAction:confirm];
    [self presentViewController:alert animated:YES completion:nil];
}

// Local Status
- (void)client:(SfuClient *)client onActiveSpeech:(BOOL)active {
    _localSpeakingIcon.hidden = !active;
    
    // 말을 중단하면 sending icon도 같이 히든처리
    if (!active) {
        _localVideoSendIcon.hidden = YES;
    }
}

- (void)client:(SfuClient *)client didRecvAllowSendingVideo:(BOOL)allow {
    if (allow) {
        if (!_localSpeakingIcon.hidden) {
            _localVideoSendIcon.hidden = NO;
        } else {
            _localVideoSendIcon.hidden = YES;
        }
    } else {
        _localVideoSendIcon.hidden = YES;
    }
}

// MARK: - SFUParticipatorDelegate

- (void)participator:(SFUParticipator*)participator onChangedSize:(CGSize)size {
    [_remoteViews reloadData];
    
    if (_fullRemoteTrack == participator.track) {
        [self determineFullRemoteViewSize:participator];
    }
}

// MARK: - Find Speaker

- (void) createSpeakerCheakingTimer {
    __weak SFURenewalViewController* weakSelf = self;
    _checkingTimer = [NSTimer scheduledTimerWithTimeInterval:FIND_LONGEST_SPEAKER_INTERVAL repeats:YES block:^(NSTimer * _Nonnull timer) {
        __strong SFURenewalViewController* strongSelf = weakSelf;
        if (!strongSelf) {
            return;
        }
        
        if (strongSelf.participators.count == 0) {
            return;
        }
        
        if (strongSelf.fixedRemoteView) {
            return;
        }

        // 가장 오랫동안 말을 하고 있는 사람을 찾아서 적용
        [strongSelf selectLongestSpeakingUser];
    }];
}

- (void) selectLongestSpeakingUser {
    SFUParticipator* longestSpeakingUser = [self findLongestSpeakingUser];
    [self selectParticipator:longestSpeakingUser];
}

-(SFUParticipator*) findLongestSpeakingUser {
    double longestSpeakingTime = DBL_MAX;
    SFUParticipator* longestSpeakingUser = _participators[0];

    for (SFUParticipator* user in _participators) {
        // 현재 보여지고 있는 사람중에서
        // 가장 오래전부터 말을 하고 있는 사람을 선택
        if(user.view.coverView.isHidden &&
           user.view.lastCoverOpenedTimeMs < longestSpeakingTime) {
            longestSpeakingUser = user;
            longestSpeakingTime = user.view.lastCoverOpenedTimeMs;
        }
    }
    return longestSpeakingUser;
}

- (void)selectParticipator:(SFUParticipator*)participator {
    if (_fullRemoteTrack != participator.track) {
        NSLogD(@"selectParticipator -> %@", participator);

        // 기존에 있던것 삭제
        [_fullRemoteTrack removeRenderer:_fullRemoteView];
        _fullRemoteTrack = nil;
        [_fullRemoteView renderFrame:nil];

        // 선택된 유저정보로 새로 연결
        _fullRemoteTrack = participator.track;
        [_fullRemoteTrack addRenderer:_fullRemoteView];
        
        [_fullRemoteView reset];
        NSString* displayName = participator.name;
        NSRange range = [participator.name rangeOfString:HIDDEN_ID];
        if (range.location != NSNotFound) {
            displayName = [participator.name substringToIndex:range.location];
        }
        _fullRemoteView.nameLabel.text = displayName;
        _fullRemoteView.hidden = (participator.track)? NO : YES;
        
        [self determineFullRemoteViewSize:participator];
    }
}

- (void) determineFullRemoteViewSize:(SFUParticipator*)participator {
    CGSize size = CGSizeEqualToSize(participator.size, CGSizeZero)? CGSizeMake(CAMERA_WIDTH, CAMERA_HEIGHT) : participator.size;

    CGRect remoteVideoFrame = AVMakeRectWithAspectRatioInsideRect(size, _fullRemoteContainerView.bounds);
    CGFloat scale = 1;
        
    if (size.width < size.height) {
        // 세로 이미지
        if (remoteVideoFrame.size.width < remoteVideoFrame.size.height) {
            scale = _fullRemoteContainerView.bounds.size.height / remoteVideoFrame.size.height;
        } else {
            scale = _fullRemoteContainerView.bounds.size.width / remoteVideoFrame.size.width;
        }
    } else {
        // 가로 이미지
        if (remoteVideoFrame.size.width > remoteVideoFrame.size.height) {
            scale = _fullRemoteContainerView.bounds.size.height / remoteVideoFrame.size.height;
        } else {
            scale = _fullRemoteContainerView.bounds.size.width / remoteVideoFrame.size.width;
        }
    }
    
    remoteVideoFrame.size.height *= scale;
    remoteVideoFrame.size.width *= scale;
    
    _fullRemoteView.frame = remoteVideoFrame;
    _fullRemoteView.center = CGPointMake(CGRectGetMidX(_fullRemoteContainerView.bounds), CGRectGetMidY(_fullRemoteContainerView.bounds));
}

// MARK: - TableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SFUParticipatorCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    SFUParticipator* user = _participators[indexPath.row];
    
    if (cell.checkMarkView.hidden == NO) {
        // 기존에 선택된 셀을 또다시 선택했다 -> 캔슬하라
        [_remoteViews deselectRowAtIndexPath:indexPath animated:NO];
        _fixedRemoteView = NO;
        
        cell.checkMarkView.hidden = YES;
    } else {
        _fixedRemoteView = YES;
        [self selectParticipator:user];
        
        cell.checkMarkView.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    SFUParticipatorCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.checkMarkView.hidden = YES;
}

// MARK: - TableViewDataSourceDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_participators count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // data source에서 index path를 보고 값을 얻어와서 해당 테이블셀에 채운다
    SFUParticipator* user = _participators[indexPath.row];
    SFUParticipatorCell* cell = [tableView dequeueReusableCellWithIdentifier:REMOTEVIEWS_CELL_ID];
    
    for (UIView *subView in [cell.view subviews] ) {
        [subView removeFromSuperview];
    }
    
    [cell.view addSubview:user.view];
    
    
    // AspectFit
    CGSize size = CGSizeEqualToSize(user.size, CGSizeZero)? CGSizeMake(CAMERA_WIDTH, CAMERA_HEIGHT) : user.size;
    CGRect remoteVideoFrame = AVMakeRectWithAspectRatioInsideRect(size, cell.view.bounds);
    CGFloat scale = 1;
    if (remoteVideoFrame.size.width < remoteVideoFrame.size.height) {
        scale = cell.view.bounds.size.height / remoteVideoFrame.size.height;
    } else {
        scale = cell.view.bounds.size.width / remoteVideoFrame.size.width;
    }
    
    remoteVideoFrame.size.height *= scale;
    remoteVideoFrame.size.width *= scale;
    
    user.view.frame = remoteVideoFrame;
    user.view.center = CGPointMake(CGRectGetMidX(cell.view.bounds), CGRectGetMidY(cell.view.bounds));
    
    return cell;
}
@end
