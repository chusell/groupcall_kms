//
//  SFUParticipatorCell.h
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFUParticipatorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkView;
@end
