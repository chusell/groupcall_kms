//
//  SFURenewalViewController.h
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SFURenewalViewControllerDelegatge
- (void) onEndWithConfigure:(NSDictionary*)dictionary;
@end

@interface SFURenewalViewController : UIViewController

@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* roomName;
@property (nonatomic, weak) id<SFURenewalViewControllerDelegatge> delegate;

@end
