//
//  SFUNewViewController.h
//  group-comm
//
//  Created by aki on 2018. 10. 8..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SFUNewViewControllerDelegatge
- (void) onEndWithConfigure:(NSDictionary*)dictionary;
@end

@interface SFUNewViewController : UIViewController

@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* roomName;
@property (nonatomic, weak) id<SFUNewViewControllerDelegatge> delegate;

@end

NS_ASSUME_NONNULL_END
