//
//  SfuClient.h
//  group-comm
//
//  Created by aki on 2018. 7. 30..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClientWebSocket.h"

#import <WebRTC/RTCCameraVideoCapturer.h>
#import "WebRTC/RTCVideoTrack.h"
#import <WebRTC/WebRTC.h>


@class SfuClient;

// MARK: -
@protocol SfuClientDelegate <NSObject>

- (void)client:(SfuClient*)client didCreateLocalCapturer:(RTCCameraVideoCapturer *)localCapturer;

- (void)client:(SfuClient*)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack name:(NSString*)name;

- (void)clientIsDisconnected:(SfuClient*)client;

- (void)client:(SfuClient*)client onError:(NSError*)error;

@optional
- (void)client:(SfuClient *)client didGetStats:(NSArray *)stats name:(NSString*)name sendonly:(BOOL)sendonly;

// for speechDetector
- (void)client:(SfuClient *)client onActiveSpeech:(BOOL)active;
- (void)client:(SfuClient *)client didRecvAllowSendingVideo:(BOOL)allow;

@end


// MARK: -
@interface SfuClient : NSObject

@property (weak, nonatomic) id<SfuClientDelegate> delegate;

- (void)joinRoom:(NSString*)room userName:(NSString*)name;

- (void)leaveRoom;


@end
