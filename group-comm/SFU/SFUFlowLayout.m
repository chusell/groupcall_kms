//
//  SFUFlowLayout.m
//  group-comm
//
//  Created by aki on 2018. 10. 11..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFUFlowLayout.h"

@implementation SFUFlowLayout

- (void)prepareLayout {
    [super prepareLayout];
    // This allows us to make intersection and overlapping
    self.minimumLineSpacing = -10;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *layoutAttributes = [super layoutAttributesForElementsInRect:rect];
    for (UICollectionViewLayoutAttributes *currentLayoutAttributes in layoutAttributes) {
        currentLayoutAttributes.zIndex = currentLayoutAttributes.indexPath.row;
        currentLayoutAttributes.frame = (currentLayoutAttributes.indexPath.row%2 == 0)? CGRectMake(currentLayoutAttributes.frame.origin.x - 40.0, currentLayoutAttributes.frame.origin.y,
                                                                                                   currentLayoutAttributes.frame.size.width, currentLayoutAttributes.frame.size.height)
                                                                                            : CGRectMake(currentLayoutAttributes.frame.origin.x + 40.0, currentLayoutAttributes.frame.origin.y,
                                                                                                         currentLayoutAttributes.frame.size.width, currentLayoutAttributes.frame.size.height);
    }
    return layoutAttributes;
}

@end
