//
//  SFUParticipatorCell.m
//  group-comm
//
//  Created by aki on 2018. 8. 28..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "SFUParticipatorCell.h"

@implementation SFUParticipatorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _checkMarkView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    // Configure the view for the selected state
    [super setSelected:selected animated:animated];
}

@end
