//
//  AppDelegate.h
//  group-comm
//
//  Created by aki on 2018. 6. 29..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

