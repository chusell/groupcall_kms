//
//  MCUViewController.m
//  group-comm
//
//  Created by aki on 2018. 6. 29..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "MCUViewController.h"
#import "McuClient.h"
#import "CaptureController.h"
#import <WebRTC/WebRTC.h>

#import "ARDStatsView.h"

@interface MCUViewController () <McuClientDelegate, RTCEAGLVideoViewDelegate>

@property (strong, nonatomic) McuClient* client;
@property (strong, nonatomic) CaptureController* captureController;
@property (strong, nonatomic) RTCVideoTrack *remoteVideoTrack;

// Views
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *remoteViewContainer;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@property (weak, nonatomic) IBOutlet UIView *highViews;
@property (weak, nonatomic) IBOutlet UIView *bottomButtons;

@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIButton *stopBtn;
@property (weak, nonatomic) IBOutlet UIButton *statsBtn;

// for Animation
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *highViewsTopconstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomButtonsConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconLeadingConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconHeightConstraint;


@property (strong, nonatomic) RTCEAGLVideoView *remoteVideoView;
@property (nonatomic) CGSize remoteVideoSize;

// StatsView
@property (strong, nonatomic) IBOutlet ARDStatsView* statsView;

@property (nonatomic) BOOL started;

@end

@implementation MCUViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _client = [[McuClient alloc] init];
    _client.delegate = self;
    
    [self setupRemoteView];
    
    _startBtn.layer.cornerRadius = 5;
    _startBtn.layer.masksToBounds = YES;
    
    _stopBtn.layer.cornerRadius = 5;
    _stopBtn.layer.masksToBounds = YES;
    
    // StatsView
    _statsView.hidden = YES;
    _statsBtn.hidden = YES;
    
    _started = NO;
    
    [self iconViewToTop:NO
                animate:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews {
    // 사이즈가 결정됨
    if (!_started) {
        [self iconViewToTop:NO
                    animate:NO];
    } else {
        [self iconViewToTop:YES
                    animate:NO];
    }
}

// MARK: -

- (void) setupRemoteView {
    if (_remoteVideoView) {
        [_remoteVideoView removeFromSuperview];
        
        _remoteVideoView.delegate = nil;
        _remoteVideoView = nil;
    }
    
    // remoteView 생성
    _remoteVideoSize = CGSizeZero;
    _remoteVideoView = [[RTCEAGLVideoView alloc] initWithFrame:CGRectZero];
    _remoteVideoView.delegate = self;
    
    [_remoteViewContainer addSubview:_remoteVideoView];
}

// MARK: Actions

- (IBAction)start:(id)sender {
    [_client start];
    
    _started = YES;
    
    // coverView animation
    [self iconViewToTop:YES
                animate:YES];
}

- (IBAction)stop:(id)sender {
    [_client stop];
    
    _started = NO;
    
    [self iconViewToTop:NO
                animate:YES];
}

// UI Actions
- (IBAction)hiddenToggle:(UIButton*)sender {
    sender.selected = !sender.selected;

    [self hiddenButtons:sender.selected];
}

- (IBAction)stats:(UIButton*)sender {
    sender.selected = !sender.selected;
    
    BOOL selected = sender.selected;
    _client.shouldGetStats = selected;
    _statsView.hidden = !selected;
}

// MARK: Private

- (void) hiddenButtons:(BOOL)hidden {
    if (hidden) {
        _bottomButtonsConstraint.constant = _bottomButtons.bounds.size.height;
        _highViewsTopconstraint.constant = -_highViews.bounds.size.height;
    } else {
        _bottomButtonsConstraint.constant = -16;
        _highViewsTopconstraint.constant = 0;
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void) iconViewToTop:(BOOL)toTop animate:(BOOL)animate {
    CGFloat alpha = 1.0;
    if (toTop) {
        _iconLeadingConstraint.constant = 20;
        _iconTopConstraint.constant = 0;
        
        _iconWidthConstraint.constant = 100;
        _iconHeightConstraint.constant = 50;
        
        alpha = 0.0;
    } else {
        _iconLeadingConstraint.constant = 0;
        _iconTopConstraint.constant = (_coverView.bounds.size.height - 100)/2;
        
        _iconWidthConstraint.constant = _coverView.bounds.size.width;
        _iconHeightConstraint.constant = 100;
        
        alpha = 1.0;
    }
    
    if (animate) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.coverView.alpha = alpha;
                             [self.view layoutIfNeeded];
                         }];
    } else {
        // alpha
        _coverView.alpha = alpha;
    }
}

- (void) setRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack {
    // 새로운 rx video track 설정됨
    if (_remoteVideoTrack == remoteVideoTrack) {
        return;
    }
    
    // 기존꺼 제거
    [_remoteVideoTrack removeRenderer:_remoteVideoView];
    _remoteVideoTrack = nil;
    [_remoteVideoView renderFrame:nil];
    
    // 새로운 연결
    _remoteVideoTrack = remoteVideoTrack;
    [_remoteVideoTrack addRenderer:_remoteVideoView];
}

// MARK: Mcu Client Delegate

-(void) mcuClient:(McuClient*)client didCreateLocalCapturer:(RTCCameraVideoCapturer *)localCapturer {
    // tx track
//    _videoCallView.localVideoView.captureSession = localCapturer.captureSession;
    _captureController = [[CaptureController alloc] initWithCapturer:localCapturer];
    [_captureController startCaptureWithWidth:1280 height:720];
}

- (void) mcuClient:(McuClient*)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack {
    // rx track
    self.remoteVideoTrack = remoteVideoTrack;
}

- (void) mcuClientIsDisconnected:(McuClient*)client {
    self.remoteVideoTrack = nil;
    
    // remoteView 초기화
    [self setupRemoteView];
    
    [_captureController stopCapture];
    _captureController = nil;
}

- (void) mcuClient:(McuClient *)client didGetStats:(NSArray *)stats {
    _statsView.stats = stats;
}

// MARK: RTCEAGLVideoViewDelegate

- (void)videoView:(RTCEAGLVideoView*)videoView didChangeVideoSize:(CGSize)size {
    if (videoView == _remoteVideoView) {
        _remoteVideoSize = size;
        
        CGRect bounds = _remoteViewContainer.bounds;
        if (_remoteVideoSize.width > 0 && _remoteVideoSize.height > 0) {
            // Aspect fill
            CGRect remoteVideoFrame = AVMakeRectWithAspectRatioInsideRect(_remoteVideoSize, bounds);
            CGFloat scale = 1;
            if (remoteVideoFrame.size.width > remoteVideoFrame.size.height) {
                // Scale by height.
                scale = bounds.size.height / remoteVideoFrame.size.height;
            } else {
                // Scale by width.
                scale = bounds.size.width / remoteVideoFrame.size.width;
            }
            
            remoteVideoFrame.size.height *= scale;
            remoteVideoFrame.size.width *= scale;
            _remoteVideoView.frame = remoteVideoFrame;
            _remoteVideoView.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
        } else {
            _remoteVideoView.frame = bounds;
        }
    }
}

@end
