//
//  McuClient.m
//  group-comm
//
//  Created by aki on 2018. 6. 29..
//  Copyright © 2018년 aki. All rights reserved.
//

#import "McuClient.h"
#import <WebRTC/WebRTC.h>


// host address
#define WS_HOST @"wss://172.23.149.120:8080/call"

@interface TimerProxy : NSObject

- (instancetype)initWithInterval:(NSTimeInterval)interval
                         repeats:(BOOL)repeats
                    timerHandler:(void (^)(void))timerHandler;
- (void)invalidate;

@end

@implementation TimerProxy {
    NSTimer *_timer;
    void (^_timerHandler)(void);
}

- (instancetype)initWithInterval:(NSTimeInterval)interval
                         repeats:(BOOL)repeats
                    timerHandler:(void (^)(void))timerHandler {
    NSParameterAssert(timerHandler);
    if (self = [super init]) {
        _timerHandler = timerHandler;
        _timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                                  target:self
                                                selector:@selector(timerDidFire:)
                                                userInfo:nil
                                                 repeats:repeats];
    }
    return self;
}

- (void)invalidate {
    [_timer invalidate];
}

- (void)timerDidFire:(NSTimer *)timer {
    _timerHandler();
}

@end


// MARK: -

@interface McuClient() <RTCPeerConnectionDelegate, RTCAudioSessionDelegate, ClientWebSocketDelegate>

@property (strong, nonatomic) ClientWebSocket* ws;

@property (strong, nonatomic) RTCPeerConnection *peerConnection;
@property (strong, nonatomic) RTCPeerConnectionFactory *peerConnectionFactory;

@property (strong, nonatomic) RTCVideoTrack *localVideoTrack;

@property (strong, nonatomic) TimerProxy* statsTimer;

@end



@implementation McuClient

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void) setup {
    [self setupWs];
    
    // webrtc log level
//    RTCSetMinDebugLogLevel(RTCLoggingSeverityInfo);
}

// MARK: - API

- (void) start {
    if (!_ws || !_ws.isOpenedWs) {
        // 아직 WebSocekt 이 오픈상태가 아니면, 오류났을 가능성이 있음
        // 기존꺼 삭제
        [self closeWs];
        
        // WebSocket 이 없으면 새로 생성해서 연결
        [self setupWs];
    }
    
    if (!_peerConnection) {
        [self setupAudioConfiguration];
        
        _peerConnectionFactory = [[RTCPeerConnectionFactory alloc] init];

        RTCConfiguration *config = [[RTCConfiguration alloc] init];
        config.iceServers = @[ [[RTCIceServer alloc] initWithURLStrings:@[ @"stun:stun.stunprotocol.org:3478" ]
                                                               username:nil
                                                             credential:nil],
                               [[RTCIceServer alloc] initWithURLStrings:@[ @"stun:stun3.l.google.com:19302" ]
                                                               username:nil
                                                             credential:nil] ];
        config.sdpSemantics = RTCSdpSemanticsUnifiedPlan;
        RTCMediaConstraints *constraints = [self defaultPeerConnectionConstraints];
        
        _peerConnection = [_peerConnectionFactory peerConnectionWithConfiguration:config
                                                                      constraints:constraints
                                                                         delegate:self];
        
        // Media Sender 생성
        [self createMediaSenders];
        
        __weak McuClient* weakSelf = self;
        [_peerConnection offerForConstraints:[self defaultMediaConstraints]
                           completionHandler:^(RTCSessionDescription * _Nullable sdp, NSError * _Nullable error) {
                               __strong McuClient* strongSelf = weakSelf;
                               if (!strongSelf) {
                                   return;
                               }
                               
                               [strongSelf.peerConnection setLocalDescription:sdp
                                                            completionHandler:^(NSError *error) {}];
                               
                               NSLogD(@"send SdpOffer");
                               NSDictionary* msg = @{ @"id" : @"client", @"sdpOffer" : sdp.sdp };
                               [strongSelf.ws sendMessage:msg];
                           }];
        

        NSLogI(@"PeerConnection created");
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
}

- (void) stop {
    if (_ws) {
        NSDictionary* msg = @{ @"id" : @"stop" };
        [_ws sendMessage:msg];
    }
    
    [self disconnect];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

// MARK: -

- (void) setupWs {
    _ws = [[ClientWebSocket alloc] init];
    _ws.delegate = self;
    [_ws setupWithURL:[NSURL URLWithString:WS_HOST]];
}

- (void) closeWs {
    if (_ws) {
        [_ws close];
        
        _ws.delegate = nil;
        _ws = nil;
    }
}

- (void) setupAudioConfiguration {
    RTCAudioSessionConfiguration *webRTCConfig = [RTCAudioSessionConfiguration webRTCConfiguration];
    webRTCConfig.categoryOptions = webRTCConfig.categoryOptions | AVAudioSessionCategoryOptionDefaultToSpeaker;
    [RTCAudioSessionConfiguration setWebRTCConfiguration:webRTCConfig];
}

- (void) disconnect {
    // 종료를 알림
    [_delegate mcuClientIsDisconnected:self];
    
    // 초기화
    _localVideoTrack = nil;;
    
    [_peerConnection close];
    _peerConnection = nil;
    _peerConnectionFactory = nil;
}

// MARK: - PeerConnection

- (RTCMediaConstraints *)defaultPeerConnectionConstraints {
    NSDictionary *optionalConstraints = @{ @"DtlsSrtpKeyAgreement" : kRTCMediaConstraintsValueTrue };
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil
                                                                             optionalConstraints:optionalConstraints];
    return constraints;
}

- (RTCMediaConstraints *)defaultMediaConstraints {
    NSDictionary *mandatoryConstraints = @{ @"OfferToReceiveAudio" : kRTCMediaConstraintsValueTrue,
                                            @"OfferToReceiveVideo" : kRTCMediaConstraintsValueTrue };
    RTCMediaConstraints* constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatoryConstraints
                                                                             optionalConstraints:nil];
    return constraints;
}

- (void)createMediaSenders {
    // Audio
    RTCMediaConstraints *constraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:nil
                                                                             optionalConstraints:nil];
    RTCAudioSource *source = [_peerConnectionFactory audioSourceWithConstraints:constraints];
    RTCAudioTrack *track = [_peerConnectionFactory audioTrackWithSource:source
                                                                trackId:@"ARDAMSa0"];
    [_peerConnection addTrack:track streamIds:@[ @"ARDAMS" ]];
    
    
    // Video
    _localVideoTrack = [self createLocalVideoTrack];
    if (_localVideoTrack) {
        [_peerConnection addTrack:_localVideoTrack streamIds:@[ @"ARDAMS" ]];
        
        // We can set up rendering for the remote track right away since the transceiver already has an
        // RTCRtpReceiver with a track. The track will automatically get unmuted and produce frames
        // once RTP is received.
        RTCVideoTrack *track = (RTCVideoTrack *)([self videoTransceiver].receiver.track);
        [_delegate mcuClient:self didReceiveRemoteVideoTrack:track];
    }
}

- (RTCVideoTrack *)createLocalVideoTrack {
    RTCVideoSource *source = [_peerConnectionFactory videoSource];
    RTCCameraVideoCapturer *capturer = [[RTCCameraVideoCapturer alloc] initWithDelegate:source];
    
    // 캡쳐러 생성됨을 VC에 알려서
    // 카메라가 켜지도록 수정
    [_delegate mcuClient:self didCreateLocalCapturer:capturer];
    
    return [_peerConnectionFactory videoTrackWithSource:source trackId:@"ARDAMSv0"];
}

- (RTCRtpTransceiver *)videoTransceiver {
    for (RTCRtpTransceiver *transceiver in _peerConnection.transceivers) {
        if (transceiver.mediaType == RTCRtpMediaTypeVideo) {
            return transceiver;
        }
    }
    return nil;
}

// MARK: - RTCPeerConnectionDelegate

/** Called when the SignalingState changed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeSignalingState:(RTCSignalingState)stateChanged {
    NSLogD(@"[didChangeSignalingState]");
}

/** Called when media is received on a new stream from remote peer. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
          didAddStream:(RTCMediaStream *)stream {
    NSLogD(@"[didAddStream]");
}

/** Called when a remote peer closes a stream. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
       didRemoveStream:(RTCMediaStream *)stream {
    NSLogD(@"[didRemoveStream]");
}

/** Called when negotiation is needed, for example ICE has restarted. */
- (void)peerConnectionShouldNegotiate:(RTCPeerConnection *)peerConnection {
    NSLogD(@"[peerConnectionShouldNegotiate]");
}

/** Called any time the IceConnectionState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeIceConnectionState:(RTCIceConnectionState)newState {
    NSLogD(@"[didChangeIceConnectionState]");
}

/** Called any time the IceGatheringState changes. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didChangeIceGatheringState:(RTCIceGatheringState)newState {
    NSLogD(@"[didChangeIceGatheringState]");
}

/** New ice candidate has been found. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didGenerateIceCandidate:(RTCIceCandidate *)candidate {
    // 여기서 서버로 candidate 정보를 보내야함
    NSLogD(@"[didGenerateIceCandidate] OnIceCandidate");

    NSDictionary* msg = @{ @"id" : @"onIceCandidate",
                     @"candidate": @{ @"candidate" : candidate.sdp,
                                         @"sdpMid" : candidate.sdpMid,
                                  @"sdpMLineIndex" : @(candidate.sdpMLineIndex) }
                           };
    [_ws sendMessage:msg];
}

/** Called when a group of local Ice candidates have been removed. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
didRemoveIceCandidates:(NSArray<RTCIceCandidate *> *)candidates {
    NSLogD(@"[didRemoveIceCandidates]");
}

/** New data channel has been opened. */
- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didOpenDataChannel:(RTCDataChannel *)dataChannel {
    NSLogD(@"[didOpenDataChannel]");
}

// MARK: - Mcu WebSocket Delegate

// received msg
- (void)processSignalMsg:(NSDictionary *)msg {
    NSString* strId = msg[@"id"];
    if ([strId isEqualToString:@"response"]) {
        NSLogD(@"[processSignalMsg] %@ : %@", strId, msg);
        
        NSString* response = msg[@"response"];
        if ([response isEqualToString:@"accepted"]) {
            RTCSessionDescription* answerSdp = [[RTCSessionDescription alloc] initWithType:RTCSdpTypeAnswer sdp:msg[@"sdpAnswer"]];
            [_peerConnection setRemoteDescription:answerSdp
                                completionHandler:^(NSError * _Nullable error) {}];
        } else {
            // 에러처리
            // peerConnection 삭제
            [self disconnect];
        }
        
    } else if ([strId isEqualToString:@"stopCommunication"]) {
        NSLogD(@"[processSignalMsg] %@ : %@", strId, msg);
        [self disconnect];
        
    } else if ([strId isEqualToString:@"iceCandidate"]) {
        NSLogD(@"[processSignalMsg] %@ : %@", strId, msg);
        
        // 파싱
        NSDictionary* candidateMsg = msg[@"candidate"];
        NSString* iceSdp = candidateMsg[@"candidate"];
        NSNumber* iceSdpMLineIndex = candidateMsg[@"sdpMLineIndex"];
        NSString* iceSdpMid = candidateMsg[@"sdpMid"];
        
        RTCIceCandidate* iceCandidate = [[RTCIceCandidate alloc] initWithSdp:iceSdp
                                                               sdpMLineIndex:[iceSdpMLineIndex intValue]
                                                                      sdpMid:iceSdpMid];
        [_peerConnection addIceCandidate:iceCandidate];
    } else {
        NSLogE(@"ERROR. Unrecognized message : %@ :%@", strId, msg);
    }
}

- (void) socketClosed {
    [self closeWs];
}

// MARK: - Stats
- (void) setShouldGetStats:(BOOL)shouldGetStats {
    if (_shouldGetStats == shouldGetStats) {
        return;
    }
    if (shouldGetStats) {
        __weak McuClient *weakSelf = self;
        _statsTimer = [[TimerProxy alloc] initWithInterval:1
                                                   repeats:YES
                                              timerHandler:^{
                                                  __strong McuClient *strongSelf = weakSelf;
                                                  [strongSelf onStatDidFire];
                                              }];
    } else {
        [_statsTimer invalidate];
        _statsTimer = nil;
    }
    _shouldGetStats = shouldGetStats;
}

- (void) onStatDidFire {
    __weak McuClient *weakSelf = self;
    [_peerConnection statsForTrack:nil
                  statsOutputLevel:RTCStatsOutputLevelDebug
                 completionHandler:^(NSArray *stats) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         __strong McuClient *strongSelf = weakSelf;
                         [strongSelf.delegate mcuClient:strongSelf didGetStats:stats];
                     });
                 }];
}

@end
