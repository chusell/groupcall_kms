//
//  McuClient.h
//  group-comm
//
//  Created by aki on 2018. 6. 29..
//  Copyright © 2018년 aki. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClientWebSocket.h"

#import <WebRTC/RTCCameraVideoCapturer.h>
#import "WebRTC/RTCVideoTrack.h"

@class McuClient;

// MARK: -
@protocol McuClientDelegate

- (void) mcuClient:(McuClient*)client didCreateLocalCapturer:(RTCCameraVideoCapturer *)localCapturer;

- (void) mcuClient:(McuClient*)client didReceiveRemoteVideoTrack:(RTCVideoTrack *)remoteVideoTrack;

- (void) mcuClientIsDisconnected:(McuClient*)client;

@optional
- (void) mcuClient:(McuClient *)client didGetStats:(NSArray *)stats;

@end


// MARK: -
@interface McuClient : NSObject

@property (weak, nonatomic) id<McuClientDelegate> delegate;

- (void) start;

- (void) stop;

@property (nonatomic) BOOL shouldGetStats;

@end


